data.coop

# Acceptable Usage Policy

20/10-2020

## 1. Introduktion
=======================================

[data.coop](https://data.coop) er en forening som har til formål at passe på medlemmernes
data. Foreningen er bygget op omkring følgende fire kerneprincipper:

-   Privatlivsbeskyttelse

-   Kryptering

-   Decentralisering

-   Zero-knowledge

Formålet med dette dokument er at tydeliggøre de rettigheder og etiske
retningslinjer, der er forbundet med brugen af [data.coop](https://data.coop)’s tjenester og
ressourcer.

-   -   -   

### 1.1 Definitioner

---------------------------------------------

|                            |                                                                                                                                                                                                                                                                                                                                                                                               |
|----------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| “Foreningen”               | Dækker over foreningen [data.coop](https://data.coop), hvis juridiske virksomhedsform er “forening”.                                                                                                                                                                                                                                                                                                               |
| “Kooperativ”               | Dækker over foreningens organiseringsform og ejerskabsforhold, i det [data.coop](https://data.coop) gerne vil etablere sig selv som et alternativ til ”de store” som f.eks. Google, Microsoft og Facebook, men vil gøre dette kooperativt – altså ejet og drevet i fællesskab, i stedet for finansieret med f.eks. venture kapital – som tilfældet for mange forholdsvis nystartede virksomheder i IT-sektoren er. |
| “Tjenester (ell. ydelser)” | Dækker over de tjenester (ell. ydelser) som [data.coop](https://data.coop) tilbyder. En oversigt over disse kan ses på [https://data.coop/tjenester/](https://https://data.coop/tjenester/) .                                                                                                                                                                                                                                                            |
| “Brugeren”                 | Dækker over den / de juridiske person(er) der benytter foreningens tjenester.                                                                                                                                                                                                                                                                                                                 |
| “AUP”                      | Acceptable Usage Policy eller *Politik for Acceptabel Brug*. Dækker over dette dokument der fastsætter de vilkår og betingelser for brug af de tjenester stillet til rådighed af foreningen [data.coop](https://data.coop).                                                                                                                                                                                        |
| “MÅ/SKAL”                  | Udtrykket dækker over et ufravigeligt krav.                                                                                                                                                                                                                                                                                                                                                   |
| “MÅ IKKE/SKAL IKKE”        | Udtrykket dækker over et ufravigeligt forbud.                                                                                                                                                                                                                                                                                                                                                 |
| “KAN”                      | Udtrykket dækker over en valgfri mulighed                                                                                                                                                                                                                                                                                                                                                     |
| “BØR”                      | Brugen af dette udtryk indikerer, at der under bestemte, specifikke omstændigheder kunne være gyldige grunde til at ignorere et bestemt emne (e.g. handle på en anden måde end indikeret), men at de fulde implikationer skal forstås og afvejes omhyggeligt, inden man eventuelt vælger en anden kurs.                                                                                       |

### 1.2 Anvendelsesområde og accept

------------------------------------------

Denne AUP gør sig gældende – og anses accepteret af brugeren – ved brug
af hjemmesiden data.coop, og de dertil knyttede tjenester stillet til
rådighed – evt. tilgået via hjemmesiden
([https://data.coop/tjenester/](https://data.coop/tjenester/)). AUP’en gælder for samtlige brugere;
besøgende og andre aktører uanset deres eksisterende tilhørsforhold til
data.coop.

## 2. AUP – Politik for Acceptabel Brug (Acceptable Usage Policy)

===========================================================================

På de følgende sider vil der blive tydeliggjort hvad der hhv. anses som
værende acceptabel brug af [data.coop](https://data.coop), uacceptabel brug af [data.coop](https://data.coop),
samt et afsnit om overtrædelser / sanktioner såfremt brugeren ikke
overholder foreningens AUP.

Det skal dog atter understreges at [data.coop](https://data.coop) er en forening og et
kooperativ, med et dertilhørende værdisæt og organisationsform. Det er i
udgangspunktet sådan at foreningens medlemmer stoler mere på hinanden
(materialiseret i foreningen) til at passe på medlemmernes data, end på
andre – oftest kommercielle platforme som f.eks. Facebook og Google.
Relationen imellem [data.coop](https://data.coop) og dens brugere – især hvis disse også er
medlemmer og derved også medejere – minder derfor meget lidt om
relationen i et mere klassisk økonomisk forhold imellem en køber og en
sælger – eller i denne kontekst – imellem en køber af en tjenesteydelse
og en profitdrevet platform som sælger denne tjenesteydelse.

### 2.1 Acceptabel brug af [data.coop](https://data.coop)
-----------------------------------
       

De indholdspolitikker, der er angivet nedenfor, er i høj grad med til at
give vores brugere en positiv oplevelse. For at [data.coop](https://data.coop) skal kunne
blive ved med at stille vores tjenester til rådighed for vores brugere,
samt udvikle og forbedre på disse, er vi nødt til at tage visse
forholdsregler mod misbrug, der på sigt kan hindre os i at levere vores
ydelser. Vi beder derfor alle om at overholde politikkerne nedenfor for
at hjælpe os med at opnå dette.

Een af [data.coop's](https://data.coop) mest basale hjørnesten er troen på at man
selv skal have kontrol over ens egen data. Denne grundsætning
danner derfor også den overordnede ramme for *acceptabel brug* af 
[data.coop's](https://data.coop) ydelser.

**2.1.1** Værende en forening og et kooperativ, [data.coop's](https://data.coop) velbefindende
er tæt knyttet til medlemmernes gøren og laden. I vores brug af
data.coop's ydelser, såvel som i interaktionen med andre medlemmer
(f.eks. på \#data.coop eller til møder, generalforsamlinger, o.l.),
forventes det af foreningens brugere at vi:

**2.1.2** Udviser empati og forståelse for andre mennesker.

**2.1.3** Udviser respekt for andre (ikke-undertrykkende) meninger
og holdninger.

**2.1.4** Er konstruktive og respektfulde i vores kommunikation
vedr. feedback – både det feedback vi måtte have til andre, men
ligeså meget det feedback vi selv ønsker at modtage.

**2.1.5** Er villige til at tage ansvar og – hvis situationen
kræver det – undskylde over for dem, der måtte være blevet berørt af
vores fejl.

**2.1.6** Fokuserer på hvad der er bedst for hele foreningen, og
ikke kun enkeltpersoner. 

**2.1.7** Deltager aktivt – i det omfang vi evner og har lyst til –
i det sociale og faglige miljø i foreningen, evt. via foreningens 
kanal på Freenode: \#data.coop og/eller på
[https://git.data.coop/](https://git.data.coop/)

**2.1.8** Det er acceptabelt at bruge de OpenSource ydelser stillet til
rådighed via [data.coop](https://data.coop) i henhold til deres egne, respektive Codes of
Conduct. 


### 2.2 Uacceptabel brug af data.coop 
---------------------------

Følgende er en ufuldstændig liste over uønsket adfærd og uacceptabel
brug af [data.coop](https://data.coop).

**2.2.1** Brug af [data.coop's](https://data.coop) tjenester til at dele, poste, lagre, eller
lign., vulgært, krænkende, pornografisk, o.l. indhold.

**2.2.2** Brug af [data.coop's](https://data.coop) tjenester til at sende / dele spam.

**2.2.3** Brug af seksualiseret sprog og/eller billedsprog samt seksuelt
grænseoverskridende adfærd af nogen slags.

**2.2.4** Igennem [data.coop's](https://data.coop) platform og/eller tjenester at praktisere
trolling, fornærmende eller nedsættende kommentarer og personlige eller
politiske angreb.

**2.2.5** Igennem [data.coop's](https://data.coop) platform at udøve offentlig eller privat
chikane.

**2.2.6** Offentliggørelse af andres private oplysninger, såsom en fysisk
eller en e-mail-adresse, uden deres udtrykkelige tilladelse.

**2.2.7** Igennem, og på, [data.coop's](https://data.coop) platform og tjenesteydelser at
promovere synspunkter og holdninger som – med rimelighed – kan tolkes at
være fascistiske, racistiske, sexistiske, homo- og/eller trans- fobiske,
militaristiske, o.l.

**2.2.8** Anden adfærd, der med rimelighed kan betragtes som upassende i en
professionel sammenhæng

## 3. Overtrædelser / sanktioner
    ============================================================

Overtrædelse af denne AUP kan medføre sanktioner og, afhængigt af
overtrædelsens alvor og/eller incidens, eksklusion af foreningen.

For nærmere specifikationer omkring hvilke sanktioner kan komme på tale
hvornår, tag venligst fat i en bestyrelsesmedlem (eller konsulter vores
Impact Guidelines). 

<!-- Vi har i øjeblikket ingen Impact Guidelines, men det kunne være et projekt som var værd at overveje måske? -->

Med det værende sagt, så opererer vi pt. med følgende
trappetrins-struktur ift. håndtering af overtrædelser:

**1.  Korrektion:** Bruges som regel i tilfælde af upassende sprog eller
    opførsel som anses for uprofessionel eller uvelkommen i foreningen.
**2.  Advarsel:** Bruges som regel i andre tilfælde end ovenstående, og
    / eller ved forskellige overtrædelser spredt over en periode.
**3.  Midlertidig eksklusion:** Bruges som regel når der er tale om en
    alvorlig overtrædelse af fællesskabsstandarder (f.eks. denne AUP),
    herunder vedvarende upassende opførsel.
**4.  Permanent eksklusion:** Bruges typisk når der er tale om et
    mønster af overtrædelser af fællesskabsstandarder, herunder
    vedvarende upassende opførsel, chikane, aggression, o.l.

### 3.1 Eksklusion
---------------------------

[data.coop's](https://data.coop) forbeholder sig retten til at nægte medlemsskab, og/eller
ekskludere eksisterende medlemmer.

Et afslag på medlemsskab af foreningen kan bl.a. komme på tale, hvis
ansøgeren har kendte forbindelser til / holdninger / synspunkter der
grundlæggende strider imod foreningens kerneprincipper og / eller klart
indikerer at disse holdninger / synspunkter vil medføre overtrædelser af
fællesskabsstandarderne.

Eksklusion kan komme på tale iht. de i afsnit 3 beskrevne situationer,
men er ikke begrænset dertil.

## 4. Ændringer af politik for acceptabel brug
    ====================================================================

Som udgangspunkt vedtages AUP'en ved en generalforsamling.

I tilfælde af force majeure hvor afholdelse af en generalforsamling er
umulig, kan bestyrelsen ekstraordinært bemyndiges til at foretage
midlertidige ændringer i AUP'en.